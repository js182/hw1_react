import React from "react";
import Button from "./components/Button";
import Modal from "./components/Modal";
import "./App.css";

class App extends React.Component {
  state = {
    isModal1Open: false,
    isModal2Open: false,
  };

  handleOpenModal1 = () => {
    this.setState({ isModal1Open: true });
  };

  handleOpenModal2 = () => {
    this.setState({ isModal2Open: true });
  };

  handleCloseModal = () => {
    this.setState({ isModal1Open: false, isModal2Open: false });
  };

  render() {
    const { isModal1Open, isModal2Open } = this.state;

    return (
        <div className="App">
          <Button
              backgroundColor="#739fa2"
              text="Open first modal"
              onClick={this.handleOpenModal1}
          />
          <Button
              backgroundColor="#7681a0"
              text="Open second modal"
              onClick={this.handleOpenModal2}
          />
          {isModal1Open && (
              <Modal
                  header="First Modal"
                  text="Content of the first modal"
                  closeButton
                  onClose={this.handleCloseModal}
                  actions={<Button text="Close" onClick={this.handleCloseModal} />}
              />
          )}
          {isModal2Open && (
              <Modal
                  header="Second Modal"
                  text="Content of the second modal"
                  closeButton
                  onClose={this.handleCloseModal}
                  actions={<Button text="Close" onClick={this.handleCloseModal} />}
              />
          )}
        </div>
    );
  }
}

export default App;
